import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store'
import router from './router'
import CommonCss from './resources/mobile/css/common.css'

Vue.config.productionTip = false

new Vue({
  store,
  vuetify,
  CommonCss,
  render: h => h(App),
  router
}).$mount('#app')
