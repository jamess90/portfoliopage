import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/home'
import NotFound from '../components/notFound'
import james from '../components_james/home_James'
import david from '../components_david/home_David'
import hwan from '../components_hwan/home_Hwan'
import hyeoksoo from '../components_hyeoksoo/home_Hyeoksoo'
import woon from '../components_woon/home_Woon'

Vue.use(VueRouter)

const router = new VueRouter({
  // mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/james', component: james },
    { path: '/david', component: david },
    { path: '/hwan', component: hwan },
    { path: '/hyeoksoo', component: hyeoksoo },
    { path: '/woon', component: woon },
    { path: '*', component: NotFound }
  ],
  scrollBehavior() {
    return {x: 0, y: 0}
  }
})

export default router
